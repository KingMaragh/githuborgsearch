package nyc.kadeem.githuborgsearch.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GithubAPIResponse {

    @SerializedName("total_count")
    @Expose
    public int totalCount;

    @SerializedName("incomplete_results")
    @Expose
    public boolean incompleteResults;

    @SerializedName("items")
    @Expose
    public List<GithubRepo> githubRepos = null;

}
