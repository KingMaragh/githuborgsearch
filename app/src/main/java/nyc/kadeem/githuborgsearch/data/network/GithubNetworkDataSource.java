package nyc.kadeem.githuborgsearch.data.network;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import java.util.List;

import nyc.kadeem.githuborgsearch.data.model.GithubAPIResponse;
import nyc.kadeem.githuborgsearch.data.model.GithubRepo;
import nyc.kadeem.githuborgsearch.data.network.retrofit.GithubService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Provides an API for requesting the server data
 */

public class GithubNetworkDataSource {

    private static final String LOG_TAG = GithubNetworkDataSource.class.getSimpleName();
    private GithubService service;

    // LiveData storing the latest downloaded github repos, and whether or not the response failed
    private final MutableLiveData<List<GithubRepo>> downloadedGithubRepos;
    private final MutableLiveData<Boolean> didResponseFail;

    public GithubNetworkDataSource(GithubService service) {
        downloadedGithubRepos = new MutableLiveData<>();
        didResponseFail = new MutableLiveData<>();
        this.service = service;
    }

    /**
     * Gets the newest github repos
     */
    public void fetchGithubRepos(String orgName) {
        Log.d(LOG_TAG, "Fetch github repositories has started");
        String formatOrgName = GithubService.SEARCH_KEYWORD + ":" + orgName;

        service.getReposForOrg(
                formatOrgName,
                GithubService.SORT_FIELD,
                GithubService.SORT_ORDER,
                GithubService.RESULTS_PER_PAGE).enqueue(new Callback<GithubAPIResponse>() {
            @Override
            public void onResponse(Call<GithubAPIResponse> call, Response<GithubAPIResponse> response) {

                if (!response.isSuccessful()) {
                    Log.d(LOG_TAG, response.message());
                    didResponseFail.postValue(true);
                    return;
                }
                didResponseFail.postValue(false);
                Log.d(LOG_TAG, response.body().githubRepos.toString());
                downloadedGithubRepos.postValue(response.body().githubRepos);
            }

            @Override
            public void onFailure(Call<GithubAPIResponse> call, Throwable t) {
                Log.e(LOG_TAG, t.getMessage());
                didResponseFail.postValue(false);
            }
        });
    }

    public LiveData<Boolean> didResponseFail() {
        return didResponseFail;
    }

    public LiveData<List<GithubRepo>> getGithubRepositories() {
        return downloadedGithubRepos;
    }
}