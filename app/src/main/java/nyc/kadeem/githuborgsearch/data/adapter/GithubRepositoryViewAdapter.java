package nyc.kadeem.githuborgsearch.data.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;


import nyc.kadeem.githuborgsearch.R;
import nyc.kadeem.githuborgsearch.data.model.GithubRepo;


public class GithubRepositoryViewAdapter extends RecyclerView.Adapter<GithubRepositoryViewAdapter.RepositoryViewHolder>{

    private List<GithubRepo> listOfRepos;

    public GithubRepositoryViewAdapter(List<GithubRepo> listOfRepos) {
        this.listOfRepos = listOfRepos;
    }

    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View repoListView = inflater.inflate(R.layout.item_github_repo_list, parent, false);

        // Return a new holder instance
        RepositoryViewHolder viewHolder = new RepositoryViewHolder(repoListView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RepositoryViewHolder holder, int position) {
        GithubRepo repo = listOfRepos.get(position);
        holder.populateTVS(repo);
    }

    @Override
    public int getItemCount() {
        return listOfRepos.size();
    }

    public void updateRepos(List<GithubRepo> newData) {
        this.listOfRepos = newData;
        notifyDataSetChanged();
    }

    public class RepositoryViewHolder extends RecyclerView.ViewHolder {
        private TextView urlTV, starsTV, forksTV, issuesTV;
        private GithubRepo currentRepo;

        public RepositoryViewHolder(View itemView) {
            super(itemView);

            CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
                    .setToolbarColor(ContextCompat.getColor(itemView.getContext(), R.color.colorPrimary))
                    .setShowTitle(true)
                    .build();

            urlTV = itemView.findViewById(R.id.repo_url);
            starsTV = itemView.findViewById(R.id.repo_stars);
            forksTV = itemView.findViewById(R.id.repo_forks);
            issuesTV = itemView.findViewById(R.id.repo_issues);

            itemView.setOnClickListener(view -> {
                customTabsIntent.launchUrl( itemView.getContext(), Uri.parse(currentRepo.htmlUrl) );
            });
        }

        public void populateTVS(GithubRepo repo) {
            setCurrentRepo(repo);
            urlTV.setText(repo.fullName);

            starsTV.setText(itemView.getContext()
                    .getString(R.string.num_of_stars, formatDecimalNumber(repo.stargazersCount)));

            forksTV.setText(itemView.getContext()
                    .getString(R.string.num_of_forks, formatDecimalNumber(repo.forksCount)));

            issuesTV.setText(itemView.getContext()
                    .getString(R.string.num_of_issues, formatDecimalNumber(repo.openIssuesCount)));
        }

        private String formatDecimalNumber(int number){
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            return formatter.format(number);
        }

        public void setCurrentRepo(GithubRepo currentRepo) {
            this.currentRepo = currentRepo;
        }


    }
}
