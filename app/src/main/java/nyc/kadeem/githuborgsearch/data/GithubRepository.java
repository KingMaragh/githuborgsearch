package nyc.kadeem.githuborgsearch.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import java.util.List;

import nyc.kadeem.githuborgsearch.data.model.GithubRepo;
import nyc.kadeem.githuborgsearch.data.network.GithubNetworkDataSource;


public class GithubRepository {
    private final GithubNetworkDataSource githubNetworkDataSource;

    private MutableLiveData<List<GithubRepo>> githubRepos;
    private MutableLiveData<Boolean> didResponseFail;

    public GithubRepository(GithubNetworkDataSource githubNetworkDataSource) {
        this.githubNetworkDataSource = githubNetworkDataSource;
        githubRepos = new MutableLiveData<>();
        didResponseFail = new MutableLiveData<>();

        updateRepositories();
        updateResponseFailure();
    }

    private void updateRepositories(){
        LiveData<List<GithubRepo>> networkData = githubNetworkDataSource.getGithubRepositories();
        networkData.observeForever(newReposFromNetwork -> {
            githubRepos.postValue(newReposFromNetwork);
        });
    }

    private void updateResponseFailure(){
        LiveData<Boolean> networkResponseFailed = githubNetworkDataSource.didResponseFail();
        networkResponseFailed.observeForever(didResponseFail -> {
            this.didResponseFail.postValue(didResponseFail);
        });
    }

    public void startFetchGithubRepoListForOrg(String orgName) {
        githubNetworkDataSource.fetchGithubRepos(orgName);
    }

    public LiveData<List<GithubRepo>> getGithubReposForOrg() {
        return githubRepos;
    }

    public LiveData<Boolean> getDidResponseFail() {
        return didResponseFail;
    }

}
