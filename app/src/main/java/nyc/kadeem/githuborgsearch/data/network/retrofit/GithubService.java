package nyc.kadeem.githuborgsearch.data.network.retrofit;

import nyc.kadeem.githuborgsearch.data.model.GithubAPIResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
    More info on the github api can be found here:
    https://developer.github.com/v3/search/#search-repositories

    Check out the Parameters section
**/

public interface GithubService {

    String SEARCH_KEYWORD = "org";
    String SORT_ORDER = "desc";
    String SORT_FIELD = "stars";
    int RESULTS_PER_PAGE = 3;

// Example URL that is built:
// https://api.github.com/search/repositories?q=org:octokit&sort=stars&order=desc&per_page=3

    @GET("/search/repositories?q=")
    Call<GithubAPIResponse> getReposForOrg(@Query("q") String org,
                                           @Query("sort") String sortByField,
                                           @Query("order") String sortOrder,
                                           @Query("per_page") int resultsPerPage);

}

