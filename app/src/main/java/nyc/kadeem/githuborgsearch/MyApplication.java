package nyc.kadeem.githuborgsearch;

import android.app.Application;

import nyc.kadeem.githuborgsearch.di.component.ApplicationComponent;
import nyc.kadeem.githuborgsearch.di.component.DaggerApplicationComponent;
import nyc.kadeem.githuborgsearch.di.module.GithubRepoListActivityModule;
import nyc.kadeem.githuborgsearch.di.module.NetworkModule;
import nyc.kadeem.githuborgsearch.di.module.StorageModule;


public class MyApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                .storageModule(new StorageModule())
                .networkModule(new NetworkModule("https://api.github.com"))
                .githubRepoListActivityModule(new GithubRepoListActivityModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}