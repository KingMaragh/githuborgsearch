package nyc.kadeem.githuborgsearch.ui.githubrepo.list;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;

import nyc.kadeem.githuborgsearch.MyApplication;
import nyc.kadeem.githuborgsearch.R;
import nyc.kadeem.githuborgsearch.data.adapter.GithubRepositoryViewAdapter;


public class GithubRepoListActivity extends AppCompatActivity {

    @Inject
    GithubListViewModelFactory factory;
    private GithubRepoListActivityViewModel viewModel;

    private RecyclerView rvGithubRepository;
    @Inject
    GithubRepositoryViewAdapter adapter;
    @Inject
    LinearLayoutManager layoutManager;
    @Inject
    DividerItemDecoration dividerItemDecoration;

    private EditText searchBar;
    private ImageButton searchButton;
    private ImageView noRepoIV;
    private TextView noRepoTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_github_repo_list);

        ((MyApplication) getApplication()).getApplicationComponent().inject(this);

        searchBar = findViewById(R.id.search_bar);
        searchButton = findViewById(R.id.search_button);
        rvGithubRepository = findViewById(R.id.rvRepos);
        noRepoTV = findViewById(R.id.no_repo_tv);
        noRepoIV = findViewById(R.id.no_repo_iv);

        viewModel = ViewModelProviders.of(this, factory).get(GithubRepoListActivityViewModel.class);

        setupRecyclerView();
        updateView();

        searchButton.setOnClickListener(view -> {
            String searchText = searchBar.getText().toString();
            searchForOrg(searchText);
        });
    }

    private void searchForOrg(String orgName) {
        viewModel.searchReposForOrg(orgName);
    }

    private void updateView() {
        viewModel.getGithubReposForOrg().observe(this, reposForOrg -> {
            adapter.updateRepos(reposForOrg);
        });

        viewModel.getResponseFailed().observe(this, failedResponse -> {
            showViewsBasedOnNoRepos(failedResponse);
        });
    }

    private void showViewsBasedOnNoRepos(boolean failed) {
        rvGithubRepository.setVisibility(failed ? View.GONE : View.VISIBLE);
        noRepoIV.setVisibility(failed ? View.VISIBLE : View.GONE);
        noRepoTV.setVisibility(failed ? View.VISIBLE : View.GONE);
    }

    private void setupRecyclerView(){
        rvGithubRepository.setAdapter(adapter);
        rvGithubRepository.setLayoutManager(layoutManager);
        rvGithubRepository.addItemDecoration(dividerItemDecoration);
    }
}

