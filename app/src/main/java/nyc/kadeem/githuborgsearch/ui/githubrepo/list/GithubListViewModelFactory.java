package nyc.kadeem.githuborgsearch.ui.githubrepo.list;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import javax.inject.Inject;

import nyc.kadeem.githuborgsearch.data.GithubRepository;


public class GithubListViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final GithubRepository githubRepository;

    @Inject
    public GithubListViewModelFactory(GithubRepository repository) {
        this.githubRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new GithubRepoListActivityViewModel(githubRepository);
    }
}