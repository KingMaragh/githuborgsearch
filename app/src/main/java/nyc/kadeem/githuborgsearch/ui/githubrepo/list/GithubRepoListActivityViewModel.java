package nyc.kadeem.githuborgsearch.ui.githubrepo.list;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import nyc.kadeem.githuborgsearch.data.GithubRepository;
import nyc.kadeem.githuborgsearch.data.model.GithubRepo;


public class GithubRepoListActivityViewModel extends ViewModel{

    private GithubRepository repository;

    public GithubRepoListActivityViewModel(GithubRepository repository) {
        this.repository = repository;
    }

    public LiveData<List<GithubRepo>> getGithubReposForOrg(){
        return repository.getGithubReposForOrg();
    }

    public LiveData<Boolean> getResponseFailed(){return repository.getDidResponseFail();}


    public void searchReposForOrg(String orgName) {
        repository.startFetchGithubRepoListForOrg(orgName);
    }

}
