package nyc.kadeem.githuborgsearch.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import nyc.kadeem.githuborgsearch.data.GithubRepository;
import nyc.kadeem.githuborgsearch.data.network.GithubNetworkDataSource;

@Module
public class StorageModule {

    @Provides
    @Singleton
    GithubRepository provideRepository(GithubNetworkDataSource networkDataSource){
        return new GithubRepository(networkDataSource);
    }

}
