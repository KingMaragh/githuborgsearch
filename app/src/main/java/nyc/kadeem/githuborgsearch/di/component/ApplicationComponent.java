package nyc.kadeem.githuborgsearch.di.component;

import javax.inject.Singleton;

import dagger.Component;
import nyc.kadeem.githuborgsearch.di.module.GithubRepoListActivityModule;
import nyc.kadeem.githuborgsearch.di.module.NetworkModule;
import nyc.kadeem.githuborgsearch.di.module.StorageModule;
import nyc.kadeem.githuborgsearch.ui.githubrepo.list.GithubRepoListActivity;


@Singleton
@Component(modules = {StorageModule.class, NetworkModule.class, GithubRepoListActivityModule.class})
public interface ApplicationComponent {

    void inject(GithubRepoListActivity githubRepoListActivity);
}
