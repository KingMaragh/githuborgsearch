package nyc.kadeem.githuborgsearch.di.module;

import android.content.Context;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import nyc.kadeem.githuborgsearch.data.GithubRepository;
import nyc.kadeem.githuborgsearch.data.adapter.GithubRepositoryViewAdapter;
import nyc.kadeem.githuborgsearch.data.model.GithubRepo;
import nyc.kadeem.githuborgsearch.ui.githubrepo.list.GithubListViewModelFactory;

@Module
public class GithubRepoListActivityModule {
    private Context context;

    public GithubRepoListActivityModule(Context context){
        this.context = context;
    }

    @Provides
    GithubListViewModelFactory provideGithubListViewModelFactory(GithubRepository repository){
        return new GithubListViewModelFactory(repository);
    }

    @Provides
    @Singleton
    public Context provideContext(){
        return context;
    }

    @Provides
    public GithubRepositoryViewAdapter provideAdapter() {
        List<GithubRepo> listOfRepos = new ArrayList<>();
        return new GithubRepositoryViewAdapter(listOfRepos);
    }

    @Provides
    public LinearLayoutManager provideLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }

    @Provides
    public DividerItemDecoration provideDividerItemDecoration(Context context, LinearLayoutManager layoutManager){
        return new DividerItemDecoration(context, layoutManager.getOrientation());
    }



}
